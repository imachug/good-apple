// Scroll credits-large.png by 2 pixels every frame

const fs = require("fs");
const PNG = require("pngjs").PNG;

(async function() {
	const png = await new Promise(resolve => {
		fs.createReadStream("credits-large.png")
			.pipe(new PNG({
				colorType: 2
			}))
			.on("parsed", function() {
				resolve(this);
			});
	});
	const buf = Buffer.concat([Buffer.alloc(256 * 3 * png.width * 4), png.data]);
	let y = 0;
	let i = 0;

	while(y < png.height + 256 * 3) {
		// y..y+256*3

		process.stderr.write(`Writing frame #${i} (from y=${y}/${png.height + 256 * 3})\n`);
		const resultingBuffer = buf.slice(y * png.width * 4, (y + 256 * 3) * png.width * 4);

		let newPng = new PNG({
			width: 256 * 4,
			height: 256 * 3,
			colorType: 2,
			bgColor: {
				red: 0,
				green: 0,
				blue: 0
			}
		});
		resultingBuffer.copy(newPng.data);

		let frameId = (6529 + i).toString();
		frameId = `frame-${"0".repeat(6 - frameId.length)}${frameId}`;
		let stream = fs.createWriteStream(`credits-scrolled/${frameId}.png`);
		await new Promise(resolve => {
			stream.on("close", resolve);
			newPng.pack().pipe(stream);
		});

		y += 3;
		i++;
	}
})();