@node constants AUDIO_EVERY >.tmp
@<.tmp set /p AUDIO_EVERY=

@node constants BUFFER_PER_SECTOR >.tmp
@<.tmp set /p BUFFER_PER_SECTOR=

@node constants AUDIO_PER_VIDEO >.tmp
@<.tmp set /p AUDIO_PER_VIDEO=

@node constants BUFFER_AUDIO_BLOCKS >.tmp
@<.tmp set /p BUFFER_AUDIO_BLOCKS=

@node constants DATA_TAIL >.tmp
@<.tmp set /p DATA_TAIL=

python -m pdpy11 --project "%~dp0demo-src" ^
	-DAUDIO_EVERY=%AUDIO_EVERY% ^
	-DBUFFER_PER_SECTOR=%BUFFER_PER_SECTOR% ^
	-DAUDIO_PER_VIDEO=%AUDIO_PER_VIDEO% ^
	-DBUFFER_AUDIO_BLOCKS=%BUFFER_AUDIO_BLOCKS% ^
	-DDATA_TAIL=%DATA_TAIL%