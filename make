#!/usr/bin/env bash
mkdir apple 2>/dev/null
node --max_old_space_size=2048 genapple
mkdir apple-raw 2>/dev/null
node --max_old_space_size=2048 pngtoraw
node --max_old_space_size=2048 gen_frame
node --max_old_space_size=2048 gen
node compact-image-converter credits.png
./compile
node tohdi