const fs = require("fs");
const assert = require("assert");

const {
	N_CYLINDERS,
	N_HEADS,
	N_SECTORS,
	SERIAL_ID,
	MODEL,
	SECTOR_LENGTH,
	BLOCK_COUNT,
	ALIGN,
	MAX_IMG_LENGTH,
	CRC_STEP,
	CRC_COUNT,
	...constants
} = require("./constants");

const MicroDOS = require("./microdos");

const firstPalette = parseInt(fs.readFileSync("first-palette.txt", "utf8"));

// Copy BootApple
fs.writeFileSync("drive/BootApple.EXE.bin", Buffer.concat([
	fs.readFileSync("demo-src/bootapple.bin"),
	fs.readFileSync("demo-src/bootloader")
]));

// Load image
let imgTotal = fs.readFileSync("img");
imgTotal = imgTotal.slice(0, imgTotal.length - imgTotal.length % SECTOR_LENGTH);

let img0 = imgTotal;
let img1 = Buffer.alloc(0);
let img2 = Buffer.alloc(0);
let img3 = Buffer.alloc(0);
if(img0.length > MAX_IMG_LENGTH) { // ~16 MB
	img1 = img0.slice(MAX_IMG_LENGTH);
	img0 = img0.slice(0, MAX_IMG_LENGTH);
}
if(img1.length > MAX_IMG_LENGTH) { // ~16 MB
	img2 = img1.slice(MAX_IMG_LENGTH);
	img1 = img1.slice(0, MAX_IMG_LENGTH);
}
if(img2.length > MAX_IMG_LENGTH) { // ~16 MB
	img3 = img2.slice(MAX_IMG_LENGTH);
	img2 = img2.slice(0, MAX_IMG_LENGTH);
}
assert(img3.length <= MAX_IMG_LENGTH);

// Load constants
let constantsBuf = Buffer.alloc(SECTOR_LENGTH, 0xFF);
constantsBuf.writeUInt16LE(img0.length / SECTOR_LENGTH   , 0o12);
constantsBuf.writeUInt16LE(CRC_STEP                      , 0o14);
constantsBuf.writeUInt16LE(CRC_COUNT                     , 0o16);
constantsBuf.writeUInt16LE(constants.LOADER_WIDTH        , 0o20);
constantsBuf.writeUInt16LE(img2.length / SECTOR_LENGTH   , 0o22);
constantsBuf.writeUInt16LE(img1.length / SECTOR_LENGTH   , 0o24);
constantsBuf.writeUInt16LE(img3.length / SECTOR_LENGTH   , 0o26);
constantsBuf.writeUInt16LE(firstPalette                  , 0o30);

for(let i = 0; i < 0o12 * 8 * CRC_COUNT; i++) {
	const sector = imgTotal.slice(i * CRC_STEP * SECTOR_LENGTH, i * CRC_STEP * SECTOR_LENGTH + SECTOR_LENGTH);
	assert(sector.length == SECTOR_LENGTH, `${sector.length} == ${SECTOR_LENGTH}`);

	let crc = 0;
	for(let j = 0; j < SECTOR_LENGTH; j += 2) {
		crc += sector.readUInt16LE(j);
	}
	crc = crc & 0xFFFF;
	constantsBuf.writeUInt16LE(crc, 0o40 + i * 2);
}

const secondaryBootloader = Buffer.concat([fs.readFileSync("demo-src/secondarybootloader"), constantsBuf]);
const credits = fs.readFileSync("demo-src/credits");

const mainDrive = new MicroDOS(__dirname + "/drive");
const ovlDrive = new MicroDOS(__dirname + "/ovldrive");

mainDrive.addFile("GoodApple.EXE", ALIGN, secondaryBootloader.length, 0o4000);
mainDrive.addFile("GoodApple0.DAT", ALIGN * 3, img0.length, 0o40000);
mainDrive.addFile("GoodApple1.DAT", ALIGN * 3 + MAX_IMG_LENGTH, img1.length, 0o40000);
mainDrive.addFile("CREDITS.EXE", ALIGN + SECTOR_LENGTH * 14, credits.length, 0o6000);
ovlDrive.addFile("GoodApple0.OVL", ALIGN, img2.length, 0o40000);
ovlDrive.addFile("GoodApple1.OVL", ALIGN + MAX_IMG_LENGTH, img3.length, 0o40000);

let stream, cnt;

function write(buf) {
	cnt += buf.length;
	stream.write(buf);
}
function invert(buf) {
	for(let i = 0; i < buf.length; i++) {
		buf[i] = buf[i] ^ 0xFF;
	}
	return buf;
}
const b = int32 => int32 >> 16;
const l = int32 => int32 & 0xFFFF;
const hc = head => (head % N_HEADS) | (Math.floor(head / N_HEADS) << 4);
const hcBlock = block => hc(1 + Math.ceil(block / N_SECTORS));


////////////////////////////////////////////////////////////////

stream = fs.createWriteStream("disk.hdi");
cnt = 0;

// Header
let header = [
	0x045a,                                         // Main configuration word                         0
	N_CYLINDERS,                                    // Cylinder count                                  1
	0xc837,                                         // Reserved                                        2
	N_HEADS,                                        // Head count                                      3
	0,                                              // Bytes per cylinder (unused)                     4
	SECTOR_LENGTH,                                  // Bytes per sector                                5
	N_SECTORS,                                      // Sectors count                                   6
	0,                                              // Seller spec (unused)                            7
	0,                                              // Seller spec (unused)                            8
	0,                                              // Seller spec (unused)                            9
	SERIAL_ID,                                      // Serial number                                   10
	1,                                              // Controller type                                 20
	1,                                              // Sectors per buffer                              21
	4,                                              // ECC size in bytes                               22
	"1v0.0.0.",                                     // Firmware version                                23
	MODEL,                                          // Model                                           27
	0x8001,                                         // 15-8 80h ??                                     47
	0,                                              // Can exchange DWORDs                             48
	0x0200,                                         // Possibilities                                   49
	0x4000,                                         // Possibilities 2                                 50
	0, 0, 0, 0, 0, 0,                               // ??                                              51
	l(N_CYLINDERS * N_HEADS * N_SECTORS + 1),       // Capacity in sectors                             60
	b(N_CYLINDERS * N_HEADS * N_SECTORS + 1),       // Capacity in sectors                             61
	0,                                              // Reserved                                        59
	l(N_CYLINDERS * N_HEADS * N_SECTORS),           // Total used sectors                              57
	b(N_CYLINDERS * N_HEADS * N_SECTORS),           // Total used sectors                              58
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // ??                                              62
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // ??                                              78
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // ??                                              94
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // ??                                              110
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // ??                                              126
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // ??                                              142
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // ??                                              158
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // ??                                              174
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // ??                                              190
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // ??                                              206
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // ??                                              222
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // ??                                              238
	0                                               // ??                                              254
];
let headerBuf = Buffer.alloc(SECTOR_LENGTH);
let headerBufPos = 0;
for(let i = 0; i < header.length; i++) {
	if(typeof header[i] == "number") {
		headerBuf.writeUInt16LE(header[i], headerBufPos);
		headerBufPos += 2;
	} else if(typeof header[i] == "string") {
		let string = Buffer.from(header[i]);
		string.swap16();
		headerBufPos += headerBuf.write(string.toString(), headerBufPos);
	} else if(Buffer.isBuffer(header[i])) {
		header[i].copy(headerBuf, headerBufPos);
		headerBufPos += header[i];
	} else {
		throw new Error(`Unknown type ${typeof header[i]}`);
	}
}
headerBuf[headerBufPos++] = 0xa5;

let headerChecksum = 0;
for(let i = 0; i < 511; i++) {
	headerChecksum += headerBuf[i];
}
headerChecksum = 0x100 - (headerChecksum & 0xFF);
headerBuf[headerBufPos++] = headerChecksum;
assert(headerBufPos == headerBuf.length);
write(headerBuf);


// MBR
write(Buffer.alloc(4096 - cnt));


// 8th sector
let sector8 = Buffer.alloc(SECTOR_LENGTH, 0xFF);
let checksum8 = 0o12701;
function writeSector8(value, address) {
	sector8.writeUInt16LE(value ^ 0xFFFF, address - 0o176000);
	checksum8 += value;
	checksum8 &= 0xFFFF;
}
writeSector8(N_CYLINDERS          , 0o176776); // Cylinder count
writeSector8(N_HEADS              , 0o176774); // Head count
writeSector8(N_SECTORS            , 0o176772); // Sectors per cylinder per documentation, sectors per head in reality
writeSector8(0x202                , 0o176770); // 2 logical disks, 2nd drive
writeSector8(hc(1)                , 0o176766); // Starts at 1st head, or 1st cylinder if there is only 1 head
writeSector8(BLOCK_COUNT          , 0o176764); // Block count
writeSector8(hcBlock(BLOCK_COUNT) , 0o176762); // Starts where 1st drive ends
writeSector8(BLOCK_COUNT          , 0o176760); // Block count
writeSector8(checksum8            , 0o176756); // Checksum
writeSector8(0o125252             , 0o176000); // Speed

write(sector8);

// Bootloader + MicroDOS root
write(Buffer.alloc(N_SECTORS * SECTOR_LENGTH - (cnt - SECTOR_LENGTH), 0xFF));
let bootloader = Buffer.alloc(SECTOR_LENGTH);
fs.readFileSync("demo-src/bootloader").copy(bootloader);
let {fileBlocks, catalogue} = mainDrive.generateBlocks(10, BLOCK_COUNT);
const totalFiles = mainDrive.getTotalFiles();
const totalBlocks = mainDrive.getTotalBlocks() + 10;
bootloader[0o30] = totalFiles & 0xFF;
bootloader[0o31] = totalFiles >> 8;
bootloader[0o32] = totalBlocks & 0xFF;
bootloader[0o33] = totalBlocks >> 8;
bootloader[0o400] = 0o123456 & 0xFF;
bootloader[0o401] = 0o123456 >> 8;
bootloader[0o402] = 0o51414 & 0xFF;
bootloader[0o403] = 0o51414 >> 8;
bootloader[0o466] = BLOCK_COUNT & 0xFF;
bootloader[0o467] = BLOCK_COUNT >> 8;
bootloader[0o470] = 10; // 10 blocks for MicroDOS root
catalogue.copy(bootloader, 0o500, 0, SECTOR_LENGTH - 0o500);
write(invert(bootloader));
write(invert(catalogue.slice(SECTOR_LENGTH - 0o500)));

// MicroDOS files
write(Buffer.alloc(N_SECTORS * SECTOR_LENGTH + 10 * SECTOR_LENGTH - (cnt - SECTOR_LENGTH), 0xFF));
write(invert(fileBlocks));

// Loader
write(Buffer.alloc(N_SECTORS * SECTOR_LENGTH + ALIGN - (cnt - SECTOR_LENGTH), 0xFF));
write(invert(secondaryBootloader));

// Credits
write(Buffer.alloc(N_SECTORS * SECTOR_LENGTH + ALIGN + SECTOR_LENGTH * 14 - (cnt - SECTOR_LENGTH), 0xFF));
write(invert(credits));
write(Buffer.alloc(48 * SECTOR_LENGTH - credits.length, 0xFF));

// Data #0
write(Buffer.alloc(N_SECTORS * SECTOR_LENGTH + ALIGN * 3 - (cnt - SECTOR_LENGTH), 0xFF));
write(invert(img0));

// Data #1
write(Buffer.alloc(N_SECTORS * SECTOR_LENGTH + ALIGN * 3 + MAX_IMG_LENGTH - (cnt - SECTOR_LENGTH), 0xFF));
write(invert(img1));

// Align second disk
let secondDisk = (
	N_SECTORS * SECTOR_LENGTH + // First disk start
	Math.ceil(BLOCK_COUNT / N_SECTORS) * N_SECTORS * SECTOR_LENGTH // First disk length
);
write(Buffer.alloc(secondDisk - (cnt - SECTOR_LENGTH), 0xFF));

// MicroDOS root
let ovlBootSector = Buffer.alloc(SECTOR_LENGTH);
let {fileBlocks: ovlFileBlocks, catalogue: ovlCatalogue} = ovlDrive.generateBlocks(20, BLOCK_COUNT);
const ovlTotalFiles = ovlDrive.getTotalFiles();
const ovlTotalBlocks = ovlDrive.getTotalBlocks() + 20;
ovlBootSector[0o30] = ovlTotalFiles & 0xFF;
ovlBootSector[0o31] = ovlTotalFiles >> 8;
ovlBootSector[0o32] = ovlTotalBlocks & 0xFF;
ovlBootSector[0o33] = ovlTotalBlocks >> 8;
ovlBootSector[0o400] = 0o123456 & 0xFF;
ovlBootSector[0o401] = 0o123456 >> 8;
ovlBootSector[0o402] = 0o51414 & 0xFF;
ovlBootSector[0o403] = 0o51414 >> 8;
ovlBootSector[0o466] = BLOCK_COUNT & 0xFF;
ovlBootSector[0o467] = BLOCK_COUNT >> 8;
ovlBootSector[0o470] = 20; // 20 blocks for MicroDOS root
ovlCatalogue.copy(ovlBootSector, 0o500, 0, SECTOR_LENGTH - 0o500);
write(invert(ovlBootSector));
write(invert(ovlCatalogue.slice(SECTOR_LENGTH - 0o500)));

// MicroDOS files
write(Buffer.alloc(secondDisk + 20 * SECTOR_LENGTH - (cnt - SECTOR_LENGTH), 0xFF));
write(invert(ovlFileBlocks));

// Data #2
write(Buffer.alloc(secondDisk + ALIGN - (cnt - SECTOR_LENGTH), 0xFF));
write(invert(img2));

// Data #3
write(Buffer.alloc(secondDisk + ALIGN + MAX_IMG_LENGTH - (cnt - SECTOR_LENGTH), 0xFF));
write(invert(img3));

write(Buffer.alloc(
	N_SECTORS * SECTOR_LENGTH + // First disk start
	Math.ceil(BLOCK_COUNT / N_SECTORS) * N_SECTORS * SECTOR_LENGTH + // First disk length
	Math.ceil(BLOCK_COUNT / N_SECTORS) * N_SECTORS * SECTOR_LENGTH - // Second disk length
	(cnt - SECTOR_LENGTH),
	0xFF
));

write(Buffer.alloc(
	N_CYLINDERS * N_HEADS * N_SECTORS * SECTOR_LENGTH - // Total size
	(cnt - SECTOR_LENGTH), // Used
	0xFF
));