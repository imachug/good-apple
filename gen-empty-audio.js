const fs = require("fs");

const header = fs.readFileSync("audio.wav").slice(0, 46);
let stream = fs.createWriteStream("audio-wave.wav");

stream.write(header);

for(let j = 0; j < 1000; j++) {
	for(let i = 1; i <= 255; i++) {
		let buf = Buffer.alloc(2);
		buf[0] = i;
		buf[1] = i;
		stream.write(buf);
	}
}

stream.end();