const fs = require("fs");
const PNG = require("pngjs").PNG;
const {AT_ONCE} = require("./constants");

let origFiles = fs.readdirSync("apple-orig");
origFiles.sort();

let replaceFiles = fs.readdirSync("apple-replace");

let addLayers = {
	iPod1: 0 + 7,
	stars1: 481 + 7,
//	iPod2: 5459 + 6 + 7 - 3,
	"iPod4-frame-003644": 3644 + 7 - 7
};

for(const path of Object.keys(addLayers)) {
	let images = fs.readdirSync(path);
	images.sort();
	addLayers[path] = {
		from: addLayers[path],
		to: addLayers[path] + images.length,
		images
	};
}

(async function() {
	for(let j = 0; j < origFiles.length; j += AT_ONCE) {
		await Promise.all(origFiles.slice(j, j + AT_ONCE).map(async (origFile, i) => {
			i += j;

			if(replaceFiles.indexOf(origFile) > -1) {
				// This frame was manually changed
				process.stdout.write(`Replacing frame ${i}\n`);
				await new Promise(resolve => {
					const stream = fs.createWriteStream(`apple/${origFile}`);
					stream.on("close", resolve);
					fs.createReadStream(`apple-replace/${origFile}`).pipe(stream);
				});
				return;
			}

			// Check that at least one layer applies
			let applies = false;
			for(const path of Object.keys(addLayers)) {
				const from = addLayers[path].from;
				const to = addLayers[path].to;

				if(from <= i && i < to) {
					applies = true;
					break;
				}
			}

			if(!applies) {
				process.stdout.write(`Copying frame ${i}\n`);
				await new Promise(resolve => {
					const stream = fs.createWriteStream(`apple/${origFile}`);
					stream.on("close", resolve);
					fs.createReadStream(`apple-orig/${origFile}`).pipe(stream);
				});
				return;
			}

			// Read
			process.stdout.write(`Parsing frame ${i}\n`);
			const frame = await new Promise(resolve => {
				fs.createReadStream(`apple-orig/${origFile}`)
					.pipe(new PNG({
						colorType: 2
					}))
					.on("parsed", function() {
						resolve(this);
					});
			});

			// Add layers
			for(const path of Object.keys(addLayers)) {
				const from = addLayers[path].from;
				const to = addLayers[path].to;

				if(from <= i && i < to) {
					// If this layer applies, add it
					const layerName = addLayers[path].images[i - from];
					process.stdout.write(`Parsing layer ${path}/${i - from}\n`);
					const layer = await new Promise(resolve => {
						fs.createReadStream(`${path}/${layerName}`)
							.pipe(new PNG({
								colorType: 2
							}))
							.on("parsed", function() {
								resolve(this);
							});
					});

					process.stdout.write(`Merging down frame ${i}\n`);
					for(let y = 0; y < 256; y++) {
						for(let x = 0; x < 256; x++) {
							let offset = (y * 256 + x) * 4;
							const [layerR, layerG, layerB] = layer.data.slice(offset, offset + 3);
							if(layerR || layerG || layerB) {
								// Set white dot
								frame.data[offset + 0] = 255;
								frame.data[offset + 1] = 255;
								frame.data[offset + 2] = 255;
							}
						}
					}
				}
			}

			process.stdout.write(`Writing frame ${i}\n`);
			frame.pack().pipe(fs.createWriteStream(`apple/${origFile}`));
		}));

		process.stdout.write("\n");
	}
})();