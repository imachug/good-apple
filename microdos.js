const fs = require("fs");
const {SECTOR_LENGTH} = require("./constants");


module.exports = class MicroDOS {
	constructor(path) {
		this.path = path;
		this.protected = {};
		this.totalProtected = 0;
		this.totalProtectedBlocks = 0;
	}

	getTotalFiles(path=null) {
		if(!path) {
			path = this.path;
		}

		const files = fs.readdirSync(path);
		let count = 0;
		for(let file of files) {
			count++;
			if(fs.statSync(`${path}/${file}`).isDirectory()) {
				count += this.getTotalFiles(`${path}/${file}`);
			}
		}
		return count + (path === this.path ? this.totalProtected : 0);
	}

	getTotalBlocks(path=null) {
		if(!path) {
			path = this.path;
		}

		const files = fs.readdirSync(path);
		let count = 0;
		for(let file of files) {
			const stat = fs.statSync(`${path}/${file}`);
			if(stat.isDirectory()) {
				count += this.getTotalBlocks(`${path}/${file}`);
			} else {
				let length = stat.size;
				if(file.toLowerCase().endsWith(".bin")) {
					length -= 4;
				}
				count += Math.ceil(length / SECTOR_LENGTH);
			}
		}
		return count + (path === this.path ? this.totalProtectedBlocks : 0);
	}

	addFile(path, address, length, memoryAddress) {
		path = `${this.path}/${path}`;
		const dirName = path.split("/").slice(0, -1).join("/");
		const fileName = path.split("/").slice(-1)[0];

		if(!this.protected[dirName]) {
			this.protected[dirName] = [];
		}
		this.protected[dirName].push({
			fileName,
			address,
			length,
			memoryAddress,
			type: "file"
		});

		this.totalProtected++;
		this.totalProtectedBlocks += Math.ceil(length / SECTOR_LENGTH);
	}


	generateBlocks(nextBlockID, diskSize) {
		let fileBlocks = [];
		let catalogue = [];
		let lastDirectory = 0;
		let blocksEnd = nextBlockID;

		const generate = (path, parent) => {
			let files;
			try {
				files = fs.readdirSync(path);
			} catch(e) {
				files = [];
			}

			for(let file of files) {
				const stat = fs.statSync(`${path}/${file}`);
				if(stat.isDirectory()) {
					// Create directory entry in catalogue
					const buf = Buffer.alloc(0o30);
					buf[0o0] = ++lastDirectory; // Directory ID
					buf[0o1] = parent; // Parent directory ID
					buf[0o2] = 0o177; // Directory
					Buffer.from(file + " ".repeat(13 - file.length)).copy(buf, 0o3); // Filename
					// Not sure about 0o20-0o30
					catalogue.push(buf);
					// Run recursively
					generate(`${path}/${file}`, lastDirectory);
				} else {
					let address = 0o40000;
					let data = fs.readFileSync(`${path}/${file}`);
					if(file.toLowerCase().endsWith(".bin")) {
						address = data.readUInt16LE(0);
						data = data.slice(4);
						file = file.replace(/\.bin$/i, "");
					}

					// Create file blocks
					const blockID = nextBlockID;
					const length = data.length;
					const blockLength = Math.ceil(length / SECTOR_LENGTH);
					fileBlocks.push(data);
					if(length % SECTOR_LENGTH) {
						fileBlocks.push(Buffer.alloc(SECTOR_LENGTH - length % SECTOR_LENGTH));
					}
					nextBlockID += blockLength;
					blocksEnd = Math.max(blocksEnd, nextBlockID);

					// Create file entry in catalogue
					const buf = Buffer.alloc(0o30);
					buf[0o0] = 0; // File type (simple)
					buf[0o1] = parent; // Parent directory ID
					Buffer.from(file + " ".repeat(14 - file.length)).copy(buf, 0o2); // Filename
					buf[0o20] = blockID & 0xFF; // Block ID
					buf[0o21] = blockID >> 8; // Block ID
					buf[0o22] = blockLength & 0xFF; // Length (in blocks)
					buf[0o23] = blockLength >> 8; // Length (in blocks)
					buf[0o24] = address & 0xFF; // Address
					buf[0o25] = address >> 8; // Address
					buf[0o26] = length & 0xFF; // Length (in bytes)
					buf[0o27] = length >> 8; // Length (in bytes)
					catalogue.push(buf);
				}
			}

			// Protected files
			for(const stat of this.protected[path] || []) {
				const file = stat.fileName;
				if(stat.type === "dir") {
					// Create directory entry in catalogue
					const buf = Buffer.alloc(0o30);
					buf[0o0] = ++lastDirectory; // Directory ID
					buf[0o1] = parent; // Parent directory ID
					buf[0o2] = 0o177; // Directory
					Buffer.from(file + " ".repeat(13 - file.length)).copy(buf, 0o3); // Filename
					// Not sure about 0o20-0o30
					catalogue.push(buf);
					// Run recursively
					generate(`${path}/${file}`, lastDirectory);
				} else {
					// Create file blocks
					const blockID = stat.address / SECTOR_LENGTH;
					const blockLength = Math.ceil(stat.length / SECTOR_LENGTH);
					const address = stat.memoryAddress;
					const length = stat.length;
					blocksEnd = Math.max(blocksEnd, blockID + blockLength);

					// Create file entry in catalogue
					const buf = Buffer.alloc(0o30);
					buf[0o0] = 1; // File type (protected)
					buf[0o1] = parent; // Parent directory ID
					Buffer.from(file + " ".repeat(14 - file.length)).copy(buf, 0o2); // Filename
					buf[0o20] = blockID & 0xFF; // Block ID
					buf[0o21] = blockID >> 8; // Block ID
					buf[0o22] = blockLength & 0xFF; // Length (in blocks)
					buf[0o23] = blockLength >> 8; // Length (in blocks)
					buf[0o24] = address & 0xFF; // Address
					buf[0o25] = address >> 8; // Address
					buf[0o26] = length & 0xFF; // Length (in bytes)
					buf[0o27] = length >> 8; // Length (in bytes)
					catalogue.push(buf);
				}
			}
		};

		generate(this.path, 0);

		// Used/free blocks
		const buf = Buffer.alloc(0o30);
		buf[0o20] = blocksEnd & 0xFF; // Used blocks
		buf[0o21] = blocksEnd >> 8; // Used blocks
		buf[0o22] = (diskSize - blocksEnd) & 0xFF; // Used blocks
		buf[0o23] = (diskSize - blocksEnd) & 0xFF; // Used blocks

		catalogue.push(buf);

		fileBlocks = Buffer.concat(fileBlocks);
		catalogue = Buffer.concat(catalogue);
		return {fileBlocks, catalogue};
	}
};