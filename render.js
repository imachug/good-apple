// Render frames.json to render-images/ as PNGs

const fs = require("fs");
const PNG = require("pngjs").PNG;
const {PAGE1, PAGE2, REG_PAGE, PALETTES} = require("./constants");

process.stderr.write("Parsing data\n");
const frames = JSON.parse(fs.readFileSync("frames.json", "utf8"));
const firstPalette = parseInt(fs.readFileSync("first-palette.txt", "utf8"));
const fileNames = fs.readdirSync("apple");

process.stderr.write("Generating screens\n");
let page0 = generateEmptyScreen();
let page1 = generateEmptyScreen();
let curPage, curPalette;
setPalette(PAGE1 + firstPalette * 0o400);

let i = 0;
(async function() {
	for(const frame of frames) {
		process.stderr.write(`Rendering frame #${i}\n`);
		for(const [value, address] of frame) {
			if(address === REG_PAGE) {
				setPalette(value);
			} else if(address < 0o100000) {
				setWord(page0, address - 0o40000, value);
			} else if(address >= 0o100000) {
				setWord(page1, address - 0o100000, value);
			}
		}
		if(curPage === 0) {
			await dumpFrame(page0);
		} else {
			await dumpFrame(page1);
		}

		i++;
	}
})();


function generateEmptyScreen() {
	return Buffer.alloc(256 * 256);
}

function setPalette(value) {
	curPage = (value & 0o100000) >> 15;
	curPalette = (value >> 8) & 0o17;
}

function setWord(frame, offset, value) {
	offset *= 4;

	for(let i = 0; i < 8; i++) {
		frame[offset] = value & 3;
		value >>= 2;
		offset++;
	}
}

function dumpFrame(data) {
	return new Promise(resolve => {
		let stream = fs.createWriteStream(`render-images/${fileNames[i]}`);
		stream.on("close", resolve);

		const png = new PNG({
			width: 256,
			height: 256,
			colorType: 2,
			bgColor: {
				red: 0,
				green: 0,
				blue: 0
			}
		});
		for(let i = 0; i < 256 * 256; i++) {
			const hex = PALETTES[curPalette][data[i]];
			const r = parseInt(hex.substr(1, 2), 16);
			const g = parseInt(hex.substr(3, 2), 16);
			const b = parseInt(hex.substr(5, 2), 16);
			png.data[i * 4 + 0] = r;
			png.data[i * 4 + 1] = g;
			png.data[i * 4 + 2] = b;
			png.data[i * 4 + 3] = 255;
		}
		png.pack().pipe(stream);
	});
}