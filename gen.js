const fs = require("fs");
const {
	SECTOR_LENGTH_WORDS,
	AUDIO_EVERY,
	AUDIO_MOVE_SPEED,
	VIDEO_PER_FRAME,
	BUFFER_PER_SECTOR,
	VIDEO_PER_SECTOR,
	FRAME_COUNT
} = require("./constants");



class SmallBuffer {
	constructor(pageSize) {
		this.pageSize = pageSize;
		this.pages = [
			{
				used: 0,
				buf: Buffer.allocUnsafeSlow(this.pageSize)
			}
		];
		this.totalAllocated = 0;
	}

	alloc(size) {
		this.totalAllocated += size;

		let page = this.pages.slice(-1)[0];
		if(page.used + size <= this.pageSize) {
			// Enough space
			page.used += size;
			return page.buf.slice(page.used - size, page.used);
		} else {
			// Not enough space
			let page = {
				used: size,
				buf: Buffer.allocUnsafeSlow(this.pageSize)
			}
			this.pages.push(page);
			return page.buf.slice(0, size);
		}
	}

	writeUInt16LE(int) {
		const buf = this.alloc(2);
		buf.writeUInt16LE(int, 0);
		return buf;
	}
}



const allocator = new SmallBuffer(16 * 1024);

function encodeWord(int) {
	return allocator.writeUInt16LE(int);
}





let frames = JSON.parse(fs.readFileSync("frames.json", "utf8"));
if(FRAME_COUNT != Infinity) {
	frames = frames.slice(0, FRAME_COUNT);
}



console.log(`Concatting`);
// Load audio
const audio = fs.readFileSync("audio.wav").slice(46);
for(let i = 0; i < audio.length; i++) {
	audio[i] = audio[i] ^ 0xFF;
}


let videoData = [];
frames.forEach(frame => {
	for(let i = 0; i < frame.length; i++) {
		videoData.push(frame[i]);
	}

	for(let i = frame.length; i < VIDEO_PER_FRAME; i++) {
		videoData.push([i, 0o34000 + Math.floor(Math.random() * 0o4000)]);
	}
});
frames = null;

console.log(`Adding audio`);

let stream = fs.createWriteStream("img");
let audioPos = 0;


function write(buf) {
	if(!stream.write(buf)) {
		return new Promise(resolve => {
			stream.once("drain", () => {
				resolve(false);
			});
		});
	} else {
		return Promise.resolve(true);
	}
}

function getAudio(pos) {
	pos = Math.round(pos) * 2;
	if(pos >= audio.length) {
		return audio.slice(-2); // To remove ticks
	} else {
		return audio.slice(pos, pos + 2); // The part we need
	}
}

async function go() {
	for(let i = 0; i < videoData.length; i += VIDEO_PER_SECTOR) {
		let ln = 0;
		for(let j = i; j < i + VIDEO_PER_SECTOR; j++) {
			if((j - i) % AUDIO_EVERY == 0) {
				// Every AUDIO_EVERY'th word is audio
				await write(getAudio(audioPos));
				ln++;
				audioPos += AUDIO_MOVE_SPEED;
			}


			const writeBuf = videoData[j];

			// Video
			const value = writeBuf ? writeBuf[0] : i % 65536;
			const address = writeBuf ? writeBuf[1] : (0o34000 + Math.floor(Math.random() * 0o4000));
			await write(encodeWord(value));
			await write(encodeWord(address));
			ln += 2;
		}

		let lastAudioPos = audioPos;
		for(let j = 0; j < BUFFER_PER_SECTOR; j++) {
			if(j % AUDIO_EVERY == 0) {
				// Every AUDIO_EVERY'th word is audio
				lastAudioPos += AUDIO_MOVE_SPEED;
			}
		}

		for(let j = 0; j < BUFFER_PER_SECTOR; j++) {
			if(j % AUDIO_EVERY == 0) {
				// Every AUDIO_EVERY'th word is audio
				await write(getAudio(audioPos));
				ln++;
				audioPos += AUDIO_MOVE_SPEED;
			}


			// Audio buffer
			await write(getAudio(lastAudioPos));
			ln++;

			lastAudioPos += AUDIO_MOVE_SPEED;
		}

		audioPos = lastAudioPos;

		while(ln < SECTOR_LENGTH_WORDS) {
			await write(encodeWord(0xBEEF));
			ln++;
		}
	}
}

(async function() {
	await go();
	console.log("Done");
	stream.end();
})();